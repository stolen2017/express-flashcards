const express = require('express');
const router = express.Router();
/*const { data } = require('../data/flashcardData.json'); */
const data = require('../data/flashcardData.json').data;              
/*const { cards } = data;*/
const cards = data.cards;

/*router.get('/hello', (req, res) => {*/
/*router.get('/cards', (req, res) => {*/
/*router.get('/', (req, res) => {*/

router.get('/', (req, res) => {
	const numberOfCards = cards.length;
	const flashcardId = Math.floor(Math.random() * numberOfCards );
	//res.redirect(`/cards/${flashcardId}?side=question`);
	res.redirect(`/cards/${flashcardId}`);
});

router.get('/:id', (req, res) => {
	/*res.send('I love treehouse');*/
	/*res.send('<h1>hello javascript</h1>');*/
	
	//const { side } = req.query;
	const side = req.query.side;
	
	//const { id } = req.params;
	const id = req.params.id;
	
	if(!side){
		//res.redirect(`/cards/${id}?side=question`);
		return res.redirect(`/cards/${id}?side=question`);
	}
	
	const name = req.cookies.username;
	
	const text = cards[id][side];
	
	//const { hint } = cards[id];
	const hint = cards[id].hint;
	//console.log(hint);
	//console.log(text);
	
	//const templateData = { text, hint };
	//const templateData = { text };
	//const templateData = { id, text };
	const templateData = { id, text, name };
	
	if (side === 'question'){
		templateData.hint = hint;
		templateData.sideToShow = 'answer';
		templateData.sideToShowDisplay = 'Answer';
	} else if ( side === 'answer' ) {
		templateData.sideToShow = 'question';
		templateData.sideToShowDisplay = 'Question';
	}
	
/*	res.render('card', { //prompt: "Who is buried Grant's tomb?",
	                     //prompt: cards[0].question,
	                       prompt: cards[req.params.id].question,
	                       
						  // hint: "Think about whose tomb it is."
						   //hint: cards[0].hint
	                        hint: cards[req.params.id].hint
	});*/
	
	res.render('card', templateData);
});

module.exports = router;
