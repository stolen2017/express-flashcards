const express = require('express');
const router = express.Router();

/*router.get('/', (request, response) => {
	response.send('I love treehouse');
});*/

/*router.use((req, res, next) => {*/
	/*console.log('One');*/
	/*console.dir('One');*/
	/*req.message = 'this message made it';*/
	/*console.log("Hello");
	const err = new Error('oh no');
	err.status = 500;
	next(err);
});*/

/*router.use((req, res, next) => {*/
	/*console.log('Two');*/
	/*console.dir('One');*/
	/*console.log(req.message);*/
/*	console.log("World");
	next();
});*/

router.get('/', (req, res) => {
	/*res.send('I love treehouse');*/
	/*res.send('<h1>I love treehouse</h1>');*/
	/*res.render('index');*/
	const name = req.cookies.username;
	if(name){
		res.render('index', { name });
	}else{
		res.redirect('/hello');
	}
});



router.get('/hello', (req, res) => {
	/*res.render('hello');*/
	const name = req.cookies.username;
	if(name){
		res.redirect('/');
	}else{
		//res.render('hello', { name: req.cookies.username} );
		res.render('hello');
	}
});

router.post('/hello', (req, res) => {
	/*console.dir(req);*/
	/*console.dir(req.body);*/
	/*res.json(req.body);*/
	/*res.render('hello');*/
	res.cookie('username', req.body.username);
	/*res.render('hello', { name: req.body.username });*/
	res.redirect('/');
});

router.post('/goodbye', (req, res) => {
	res.clearCookie('username');
	res.redirect('hello');
});

module.exports = router;